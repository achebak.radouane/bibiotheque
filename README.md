# Bibiotheque

## Auteur
- Nom : ACHEBAK
- Prénom : Radouane
- Métier : IT

## Description
Project de test

## Installation
Lancer le script "install.sh"
• Ajouter un fichier dans le 3e commit : install.sh :

#!/bin/bash
echo "Installation completed !"

## Désinstallation
Lancer le script **uninstall.sh**
• Ajouter un fichier dans le 3e commit : uninstall.sh :

#!/bin/bash
echo "Remove completed !"

## Patch
Lancer le script "patch.sh"

#!/bin/bash
echo "Patch completed !"


